<?php
namespace App\Http\Controllers\Admin;
use App\Event;
use App\Services\AdminService;
use App\Task;
use App\TaskFile;
use App\TaskNote;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\User;
class AdminController extends Controller
{
    public $adminService;

    public function __construct(AdminService $adminService)
    {
        $this->adminService = $adminService;
    }
    public function getIndex()
    {
        $admin=auth()->guard('admin')->user();
        return view('admin.dashboard',compact('admin'));
    }
    public function editAdminCreditionals(Request $request){
        $data = $request->validate([
            'nickname' => [ 'string', 'unique:users', 'alpha_num','nullable'],
            'password' => 'string|min:6|nullable',
            'email' => 'string|email|nullable',
            'first_name' => 'string|nullable',
            'second_name' => 'string|nullable',
            'photo' => 'image|dimensions:max_width=2000,max_height=2000|nullable',
        ]);
        $file = $request->file('photo');
        $admin=auth()->guard('admin')->user();
        $this->adminService->editCreditionals($data,$file,$admin);
        return redirect()->back();
    }
    public function getIndexUser($userId)
    {
        $user=User::find($userId);
        return view('admin.dashboard',compact('user'));
    }

}