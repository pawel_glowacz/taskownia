<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/admin';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout', 'getLogout']]);
    }

    public function getAdminLogin()
    {
        if (auth()->guard('admin')->user()) return redirect()->route('admin.dashboard');
        return view('admin.login');
    }

    public function adminAuth(Request $request)
    {

        $this->validate($request, [
            'login' => 'required|max:100',
            'password' => 'required|max:100',
        ]);
        if (auth()->guard('admin')->attempt(['nickname' => $request->input('login'), 'password' => $request->input('password')])) {
            return redirect()->route('admin.dashboard.user',['userId'=>1]);
        } else {
            dd('Dane nie prawidłowe');
        }

    }

    public function getLogout()
    {
        auth()->guard('admin')->logout();
        return redirect()->route('admin.login');
    }

}