<?php
namespace App\Http\Controllers\Admin;
use App\Event;
use App\Services\AdminService;
use App\Option;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
class OptionController extends Controller
{
    public function getHours(Request $request)
    {
        $globalStart = Option::select('value')
            ->where('key','globalStart')
            ->pluck('value');
        $globalEnd = Option::select('value')
            ->where('key','globalEnd')
            ->pluck('value');
        return response()->json([
            'globalStart' => $globalStart,
            'globalEnd' => $globalEnd
        ]);
    }

    public function updateHours(Request $request)
    {
        $globalStart = Option::where('key','globalStart')->first();
        $globalStart->value=$request->globalStart;
        $globalStart->update();
        $globalEnd = Option::where('key','globalEnd')->first();
        $globalEnd->value=$request->globalEnd;
        $globalEnd->update();

        return response()->json('ok');
    }

}