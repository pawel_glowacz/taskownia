<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Option;
use App\Services\TaskService;
use App\Task;
use App\TaskFile;
use App\TaskLog;
use App\TaskNote;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use PhpParser\Node\Stmt\Return_;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Debug\Debug;

class TaskController extends Controller
{
    public $taskService;

    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    public function getTaskUser($userId, $taskId)
    {
        $user = User::find($userId);
        $tasks = Task::all();
        $events = $tasks->where('type', '0');
        $waitings = $tasks->where('type', '1');
        $templates = $tasks->where('type', '2');
        $employees = User::all();
        $taskFiles = TaskFile::where('task_id', $taskId)->get();
        $taskNotes = TaskNote::where('task_id', $taskId)->get();
        return view('admin.dashboard', compact('events', 'user', 'employees', 'templates', 'waitings', 'admin', 'taskFiles', 'taskNotes'));
    }

    public function loadTaskUser($userId, $taskId)
    {
        $user = User::find($userId);
        $tasks = Task::all();
        $events = $tasks->where('type', '0');
        $waitings = $tasks->where('type', '1');
        $templates = $tasks->where('type', '2');
        $employees = User::all();
        $taskFiles = TaskFile::where('task_id', $taskId)->get();
        $taskNotes = TaskNote::where('task_id', $taskId)->get();
        return view('partials.task.task', compact('events', 'user', 'employees', 'templates', 'waitings', 'admin', 'taskFiles', 'taskNotes'));
    }

    public function getTaskId(Request $request)
    {
        $taskId = $request->id;
        $task = Task::find($taskId);
        $taskFiles = TaskFile::select('task_files.created_at', 'task_files.ext', 'task_files.mime', 'task_files.id', 'task_files.path', 'users.first_name', 'users.second_name')
            ->leftJoin('users', 'users.id', 'task_files.user_id')
            ->where('task_files.project_id', $task->project_id)->get();
        $taskNotes = TaskNote::select('task_notes.id', 'task_notes.created_at', 'task_notes.body', 'users.first_name', 'users.second_name')
            ->leftJoin('users', 'users.id', 'task_notes.user_id')
            ->where('task_notes.project_id', $task->project_id)->get();
        $taskLog = TaskLog::select('task_logs.created_at', 'task_logs.id', 'task_logs.title as logTitle', 'users.first_name', 'users.second_name', 'tasks.title as taskTitle')
            ->leftJoin('users', 'users.id', 'task_logs.user_id')
            ->leftJoin('tasks', 'tasks.id', 'task_logs.task_id')
            ->where('task_logs.project_id', $task->project_id)->get();
        $projects = Task::where('project_id',$task->project_id)
            ->where('kind', 0)
            ->where('end','<', now())
            ->get();
        $workedD = 0;
        $workedH = 0;
        $workedM= 0;
        foreach($projects as $event){
            $start = $event->start;
            $end = $event->end;
            $durationActualD = $start->diff($end)->format('%d');
            $durationActualH = $start->diff($end)->format('%h');
            $durationActualM = $start->diff($end)->format('%m');
            $workedD += $durationActualD;
            $workedH += $durationActualH;
            $workedM += $durationActualM;
        }
        $task->workedD=$workedD;
        $task->workedH=$workedH;
        $task->workedM=$workedM;
        $eventsJson = array(
            'task' => $task,
            'notes' => $taskNotes,
            'attachments' => $taskFiles,
            'logs' => $taskLog,
        );
        return response()->json($eventsJson);
    }


    public function getTasks(Request $request)
    {
        $user = User::find($request->id);
        $events = Task::where('user_id',$user->id)
            ->where('type',0)
            ->get();

        $eventsJson = array();
        foreach ($events as $event) {
            $eventsJson[] = array(
                'id' => $event->id,
                'title' => $event->title,
                'color' => $event->color,
                'start' => date($event->start),
                'end' => date($event->end),
                'client' => $event->client,
                'type' => $event->type,
                'deadline' => date($event->deadline),
                'duration' => $event->duration,
                'color' => $event->color,
                'editable' => $event->editable,
                'project_id' => $event->project_id,
            );
        }
        return response()->json($eventsJson);
    }

    public function stopTask(Request $request)
    {
        $task = Task::find($request->id);
        $task->end=$request->end;
        $task->status=0;
        $task->update();
        return response()->json($task);
    }
    public function pauseTask(Request $request)
    {
        $task = Task::find($request->id);
        $task->end=$request->end;
        $task->update();
        return response()->json($task);
    }
    public function startTask(Request $request)
    {
        $task = Task::find($request->id);
        return response()->json($task);
    }

    public function getSidebarTasks(Request $request)
    {
        $waitings = Task::select('tasks.*', 'users.first_name', 'users.second_name')
            ->leftJoin('users', 'users.id', 'tasks.user_id')
            ->where('tasks.type', '1')
            ->where('tasks.user_id', $request->userId)
            ->get();

        $templates = Task::select('tasks.*', 'users.first_name', 'users.second_name')
            ->leftJoin('users', 'users.id', 'tasks.user_id')
            ->where('tasks.type', '2')
            ->where('tasks.user_id', $request->userId)
            ->get();
        return response()->json([
            'waitings' => $waitings,
            'templates' => $templates
        ]);
    }

    public function editEventCalendar(Request $request)
    {
        $event = Task::find($request->id);
        $event->start = $request->start;
        $event->end = $request->end;
        $event->color = $request->color;
        $event->editable = $request->editable;
        $event->update();
        return response()->json("ok");
    }

    public function addEventCalendar(Request $request)
    {
        $event = new Task();
        $event->title = $request->title;
        $event->deadline = $request->deadline;
        $event->client = $request->start;
        $event->start = $request->start;
        $event->end = $request->end;
        $event->save();
        return response()->json($event);
    }

    public function addTask(Request $request)
    {
        $data = $request->validate([
            'title' => 'string|required',
            'client' => 'string|nullable',
            'duration' => 'integer|nullable',
            'deadline' => 'date|nullable',
            'project_id' => 'integer|nullable',
            'date' => 'date|date_format:Y-m-d|nullable',
            'time' => 'date_format:H:i|nullable',
            'type' => 'integer|required',
            'start' => 'date|nullable',
            'end' => 'date|nullable',
        ]);
        $data = $request->all();
        \Log::debug($data);
        $user = User::find($request->userId);
        $task = $this->taskService->create($data, $user);
        $task->first_name=$task->user->first_name;
        $task->second_name=$task->user->second_name;
        return response()->json([
            'task' => $task
        ]);
    }

    public function editTask(Request $request)
    {
        $data = $request->validate([
            'title' => 'string|required',
            'client' => 'string|nullable',
            'duration' => 'integer|nullable',
            'date' => 'date|date_format:Y-m-d|nullable',
            'time' => 'date_format:H:i|nullable'
        ]);
        $data = $request->all();
        $user = User::find($request->userId);
        $this->taskService->create($data, $user);
        return redirect()->back();
    }

    public function deleteTask(Request $request)
    {
        $task = Task::find($request->id);
        $task->delete();
        return \Response::json("ok");
    }

    public function addTaskNote(Request $request)
    {
        $data = $request->validate([
            'body' => 'string|required'
        ]);
        $user = User::find($request->userId);
        $task = Task::find($request->taskId);
        $note = $this->taskService->addNote($data, $user, $task);
        $taskNote = TaskNote::select('task_notes.id', 'task_notes.created_at', 'task_notes.body', 'users.first_name', 'users.second_name')->leftJoin('users', 'users.id', 'task_notes.user_id')->where('task_notes.id', $note->id)->get();
        $json = array(
            'note' => $taskNote
        );
        return \Response::json($json);
    }

    public function editTaskNote(Request $request)
    {
    }

    public function deleteTaskNote(Request $request)
    {
        $taskNote = TaskNote::find($request->id);
        $taskNote->delete();
        return response()->json("ok");
    }

    public function addTaskFile(Request $request)
    {
        $data = $request->validate([
            'file' => 'required',
            'userId' => 'integer|required',
            'taskId' => 'integer|required',
        ]);
        $file = $request->file;
        $user = User::find($request->userId);
        $task = Task::find($request->taskId);
        $attachment = $this->taskService->addAttachment($file, $user, $task);
        $taskFile = TaskFile::select('task_files.created_at', 'task_files.ext', 'task_files.mime', 'task_files.id', 'task_files.path', 'users.first_name', 'users.second_name')
            ->leftJoin('users', 'users.id', 'task_files.user_id')
            ->where('task_files.id', $attachment->id)->get();
        return response()->json([
            'attachment' => $taskFile
        ]);
    }

    public function editTaskFile(Request $request)
    {
    }

    public function downloadTaskFile(Request $request, $id)
    {
        $taskFile = TaskFile::find($id);
        return response()->download('img/files/' . $taskFile->path);
    }

    public function deleteTaskFile(Request $request)
    {
        $taskFile = TaskFile::find($request->id);
        \File::delete('img/files/' . $taskFile->path);
        $taskFile->delete();
        return response()->json("ok");
    }

    public function searchTask(Request $request)
    {
        $tasks = Task::where('user_id', $request->userId)
            ->where('title','like',$request->search.'%')
            ->where('type','=','0')
            ->get();
        return response()->json($tasks);
    }

}