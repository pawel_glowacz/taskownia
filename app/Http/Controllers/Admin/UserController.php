<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Services\UserService;
use App\UserHours;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function addUser(Request $request)
    {
        $data = $request->validate([
            'nickname' => ['required', 'string', 'unique:users', 'alpha_num'],
            'password' => 'required|string|min:6',
            'first_name' => 'string|nullable',
            'second_name' => 'string|nullable',
            'type' => 'boolean|nullable',
            'stand' => 'string|nullable',
            'pay_standard' => 'integer|nullable',
            'pay_overtime' => 'integer|nullable',
            'photo' => 'image|dimensions:max_width=2000,max_height=2000|nullable',
        ]);
        $file = $request->file('photo');
        $user = $this->userService->create($data, $file);
        return response()->json([
            'user' => $user
        ]);
    }

    public function editUserCreditionals(Request $request)
    {
        $data = $request->validate([
            'nickname' =>  "string|unique:users,nickname,$request->userId|alpha_num|nullable",
            'password' => 'string|min:6|nullable',
            'email' => 'string|email|nullable',
            'first_name' => 'string|nullable',
            'second_name' => 'string|nullable',
            'type' => 'boolean|nullable',
            'stand' => 'string|nullable',
            'pay_standard' => 'integer|nullable',
            'pay_overtime' => 'integer|nullable',
            'photo' => 'image|dimensions:max_width=2000,max_height=2000|nullable',
        ]);
        $file = $request->file('photo');
        $user = User::find($request->userId);
        $edited = $this->userService->editCreditionals($data, $file, $user);
        return response()->json([
            'user' => $edited
            ]);
    }
    public function getTimetable(Request $request){
        $user=User::find($request->id);
        $data=array();
        $i=0;
        foreach($request->data as $d){
            $data[$i]['start'] = UserHours::select('start')->where('start','LIKE',date('Y-m-d',strtotime($d['start'])).'%' )->orderBy('id','desc')->pluck('start')->first();
            if( empty($data[$i]['start']))  $data[$i]['start'] = $d['start'];
            $data[$i]['end'] = UserHours::select('end')->where('end','LIKE',date('Y-m-d',strtotime($d['end'])).'%' )->orderBy('id','desc')->pluck('end')->first();
            if( empty($data[$i]['end']))  $data[$i]['end'] = $d['end'];

            $data[$i]['color'] = '#000000';

            $i++;
        }
        if(!$request->ajax()) {
            return redirect()->back();
        } else {
            echo json_encode(array(
                'data' => $data
            ));
        }
    }
    public function getTimetableHours(Request $request){
        $user=User::find($request->id);

        $begin = new \DateTime($request->start);
        $end = new \DateTime($request->end);

        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($begin, $interval, $end);

        $i=0;
        foreach ($period as $dt) {
          $d = $dt->format("Y-m-d");
          $data[$i]['start'] = UserHours::select('start')->where('start','LIKE',date('Y-m-d',strtotime($d)).'%' )->orderBy('id','desc')->pluck('start')->first();

          $data[$i]['end'] = UserHours::select('end')->where('end','LIKE',date('Y-m-d',strtotime($d)).'%' )->orderBy('id','desc')->pluck('end')->first();
          $data[$i]['color'] = '#000000';
          if( empty($data[$i]['start']) || empty($data[$i]['end']))  unset($data[$i]);
          // if( empty($data[$i]['end']))  $data[$i]['end'] = $d['end'];



          $i++;
        }

        if(!$request->ajax()) {
            return redirect()->back();
        } else {
            echo json_encode($data);
        }
    }
    public function getUsers(){
        $users = User::all();
        return response()->json([
            'users' => $users
        ]);
    }
    public function getUserId(Request $request){
        $users = User::find($request->id);
        return response()->json([
            'users' => $users
        ]);
    }
    public function updateTimetable(Request $request){
        $user=User::find($request->id);
        foreach($request->data as $data){
            $old = UserHours::where('start','LIKE',date('Y-m-d',strtotime($data['start'])).'%' );
            $old->delete();
            $timetable= new UserHours();
            $timetable->user()->associate($user);
            $timetable->start = date('Y-m-d H:i:s',strtotime($data['start']));
            $timetable->end = date('Y-m-d H:i:s',strtotime($data['end']));
            $timetable->save();
        }
        if(!$request->ajax()) {
            return redirect()->back();
        } else {
            echo json_encode(array(
            ));
        }

    }
//    public function deletePhoto(Request $request)
//    {
//        $id = Input::get('id');
//        $photo = Images::find($id);
//        if(file_exists('public/img/' . $photo->filename)) {
//            unlink('public/img/' . $photo->filename);
//        }
//        $photo->delete();
//        return json_encode(array('true'));
//    }
}
