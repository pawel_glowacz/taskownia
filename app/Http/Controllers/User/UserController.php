<?php
namespace App\Http\Controllers\User;
use App\Event;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\User;
class UserController extends Controller
{
    public function getIndex()
    {
        $tasks = Task::all();
        return view('admin.dashboard',compact('tasks'));
    }
}