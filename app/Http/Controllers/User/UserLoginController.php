<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class UserLoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/user';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function getUserLogin()
    {
        return view('user.login');
    }

    public function userAuth(Request $request)
    {
        $this->validate($request, [
            'login' => 'required',
            'password' => 'required',
        ]);
        if (auth()->attempt(['nickname' => $request->input('login'), 'password' => $request->input('password')])) {
            return redirect()->route('user.dashboard');

        } else {
            dd('Błędne dane');
        }
    }
}