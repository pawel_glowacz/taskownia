<?php

namespace App;

use function Deployer\has;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'email', 'nickname','first_name','second_name','stand','type','pay_standard','pay_overtime','photo'
    ];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function admin(){
        return $this->belongsTo(Admin::class);
    }
    public function tasks(){
        return $this->hasMany(Task::class);
    }
    public function taskFile(){
        return $this->hasMany(TaskFile::class);
    }
    public function taskNote(){
        return $this->hasMany(TaskNote::class);
    }
    public function timetable(){
        return $this->hasMany(UserHours::class);
    }
}
