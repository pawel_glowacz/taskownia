<?php
namespace App\Services;

use App\Admin;
use App\User;
use Intervention\Image\Facades\Image;

class AdminService
{
    public function editCreditionals($data,$photo,Admin $admin){
        $admin->fill($data);
        if(!empty($data['password'])) {
            $admin->password = bcrypt($data['password']);
        }
        if ($photo) { // TODO: Photo update dont works
            $filename = time().'-'.$photo->getClientOriginalName();
            $image_resize = Image::make($photo);
            $image_resize->resize(100, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image_resize->save('img/photos/'.$filename);
            $admin->photo = $filename;
        }
        $admin->save();
        return $admin;
    }
}