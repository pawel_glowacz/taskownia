<?php

namespace App\Services;

use App\Admin;
use App\Event;
use App\Task;
use App\TaskFile;
use App\TaskNote;
use App\User;use Intervention\Image\Facades\Image;


class TaskService
{
    public function create($data, User $user = null, Admin $admin = null)
    {
        $task = new Task();
        $task->fill($data);
        $task->user()->associate($user);
        $task->admin()->associate($admin);
//        $task->color = $data['color'];


        if(isset($data['duration'])) {
            if($data['duration'] == 0) {

                $task->duration = 3600000;
        }
        }
        if(isset($data['date'])) {
        if($data['date'] != null) {
            $date = $data['date'];
            $time = $data['time'];
            $task->deadline = date('Y-m-d H:i:s', strtotime("$date $time"));
        }
        }
        $lastTask = Task::orderBy('project_id','desc')
            ->first();
        if($task->type != 0){
            if(!$lastTask){
                $task->project_id = 1;
            }else{
                $task->project_id=$lastTask->project_id + 1 ;
            }
        }

        $task->save();
        return $task;
    }

    public function editTask($data,Task $task, User $user = null, Admin $admin = null)
    {
        $task->fill($data);
        return $task;
    }

    public function addNote($data, User $user, Task $task)
    {
        $note = new TaskNote();
        $note->fill($data);
        $note->user()->associate($user);
        $note->task()->associate($task);
        $note->project_id=$task->project_id;
        $note->save();
        return $note;
    }

    public function editNote($data, User $user, TaskNote $note)
    {
        $note->fill($data);
        $note->user()->associate($user);
        $note->update();
        return $note;
    }

    public function deleteNote(TaskNote $note)
    {
        $note->delete();
        return true;
    }
    public function addAttachment($fileRequest, User $user, Task $task)
    {
        $file = new TaskFile();
        $file->user()->associate($user);
        $file->task()->associate($task);
        $file->project_id=$task->project_id;
        if ($fileRequest) {
            $filename = time().'-'.$fileRequest->getClientOriginalName();
            $fileRequest->move('img/files/', $filename);
            $file->path = $filename;
            $file->mime = $fileRequest->getClientMimeType();
            $file->ext = \File::extension($filename);;
        }
        $file->save();
        return $file;
    }

    public function editAttachment($data, User $user, TaskFile $file)
    {
        $file->fill($data);
        $file->user()->associate($user);
        $file->update();
        return $file;
    }

    public function deleteAttachment(TaskFile $file)
    {
        $file->delete();
        return true;
    }
}