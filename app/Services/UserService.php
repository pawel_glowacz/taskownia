<?php
namespace App\Services;

use App\Admin;
use App\User;
use Intervention\Image\Facades\Image;

class UserService{
    public function create($data,$photo){
        $admin=Admin::find(1); // TODO: Change to actual admin
        $user = new User();
        $user->fill($data);
        $user->password = bcrypt($data['password']);
        $user->admin()->associate($admin);
        if ($photo) {
            $filename = time().'-'.$photo->getClientOriginalName();
            $image_resize = Image::make($photo);
            $image_resize->resize(100, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image_resize->save('img/photos/'.$filename);
            $user->photo = $filename;
        }
        $user->save();
        return $user;
    }
    public function editCreditionals($data,$photo,User $user){
        $user->fill($data);
        if(!empty($data['password'])) {
            $user->password = bcrypt($data['password']);
        }
        if ($photo) { // TODO: Photo update dont works
            $filename = time().'-'.$photo->getClientOriginalName();
            $image_resize = Image::make($photo);
            $image_resize->resize(100, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image_resize->save('img/photos/'.$filename);
            $user->photo = $filename;
        }
        $user->save();
        return $user;
    }
    public function addHours($data,User $user){}
    public function editHours($data,User $user){}
}