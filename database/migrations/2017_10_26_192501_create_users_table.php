<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
            $table->rememberToken();
            $table->string('email')->nullable();
            $table->string('nickname');
            $table->string('password');
            $table->string('first_name')->nullable();
            $table->string('second_name')->nullable();
            $table->string('stand')->nullable();
            $table->string('type')->nullable();
            $table->string('pay_standard')->nullable();
            $table->string('pay_overtime')->nullable();
            $table->string('photo')->nullable();
            $table->unsignedInteger('admin_id')->unsigned();
            $table->softDeletes();

        });
        Schema::table('users', function($table) {
            $table->engine = 'InnoDB';
            $table->foreign('admin_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
