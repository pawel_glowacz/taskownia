<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title');
            $table->string('color')->default("rgb(9, 103, 9)");
            $table->integer('project_id')->nullable();
            $table->string('client')->nullable();
            $table->integer('type')->default(0);
            $table->integer('kind')->default(0);
            $table->timestamp('deadline')->nullable();
            $table->timestamp('start')->nullable();
            $table->timestamp('end')->nullable();
            $table->integer('duration')->nullable()->default(3600000);
            $table->integer('status')->default(1);
            $table->boolean('editable')->default(true);
            $table->unsignedInteger('user_id')->unsigned()->nullable();
            $table->unsignedInteger('admin_id')->unsigned()->nullable();
            $table->softDeletes();
        });
        Schema::table('tasks', function($table) {
            $table->engine = 'InnoDB';
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('admin_id')->references('id')->on('admins');
        });
//        DB::statement('ALTER TABLE tasks ADD duration_hours1 INT UNSIGNED ZEROFILL NOT NULL');


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
