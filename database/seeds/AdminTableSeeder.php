<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new \App\Admin();
        $admin->first_name = 'Pawel';
        $admin->second_name= 'Glowacz';
        $admin->nickname = 'pawelsome';
        $admin->email = 'test@test.com';
        $admin->password = bcrypt('superadmin');
        $admin->role = 0;
        $admin->photo = '1511794574-logo.png';
        $admin->save();
    }
}
