<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new \App\Option();
        $data->key="globalStart";
        $data->value='10:00';
        $data->save();

        $data = new \App\Option();
        $data->key='globalEnd';
        $data->value='18:00';
        $data->save();
    }
}
