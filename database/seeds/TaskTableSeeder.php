<?php

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data= new \App\Task();
        $data->title="Task 1";
        $data->start= now();
        $data->end= now()->addHours(3);
        $data->deadline= now()->addHours(3);
        $data->user_id= 1;
        $data->admin_id= 1;
        $data->project_id= 1;
        $data->type= 1;
        $data->save();

        $data= new \App\Task();
        $data->title="Task 1";
        $data->start= now()->addHours(-4);
        $data->end= now()->addHours(-2);
        $data->deadline= now()->addHours(-2);
        $data->user_id= 1;
        $data->admin_id= 1;
        $data->project_id= 2;
        $data->type= 1;
        $data->save();

        $data= new \App\Task();
        $data->title="Task 1";
        $data->start= now()->addHours(-25);
        $data->end= now()->addHours(-23);
        $data->deadline= now()->addHours(-23);
        $data->user_id= 1;
        $data->admin_id= 1;
        $data->project_id= 3;
        $data->type= 1;
        $data->save();

        $data= new \App\Task();
        $data->title="Task 1";
        $data->start= now()->addHours(-48);
        $data->end= now()->addHours(-45);
        $data->deadline= now()->addHours(-45);
        $data->user_id= 1;
        $data->admin_id= 1;
        $data->project_id= 4;
        $data->type= 2;
        $data->save();
    }
}
