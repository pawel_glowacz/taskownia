<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new \App\User();
        $admin->first_name = 'Pawel';
        $admin->second_name= 'Glowacz';
        $admin->nickname = 'test';
        $admin->email = 'test@test.com';
        $admin->stand = 'Grafik';
        $admin->stand = 1;
        $admin->pay_standard = 1;
        $admin->pay_overtime = 2;
        $admin->photo = "1511794574-logo.png";
        $admin->type = 1;
        $admin->password = bcrypt('superadmin');
        $admin->admin_id = 1;
        $admin->save();

    }
}
