<?php
namespace Deployer;
require 'recipe/laravel.php';

// Configuration

set('ssh_type', 'native');
set('ssh_multiplexing', false);

set('repository', 'pawel_glowacz@bitbucket.org:pawel_glowacz/taskownia.git');

add('shared_files', [
    '.env'
]);

add('shared_dirs', [
    'public/img'
]);

add('writable_dirs', [
    'public/img'
]);

// Servers

server('dev', 'pawelglowacz.pl')
    ->user('web')
    ->set('branch', 'master')
    ->identityFile()
    ->set('deploy_path', '/home/web/taskownia')
    ->pty(true);


// Tasks

desc('Restart PHP-FPM service');
task('php-fpm:restart', function () {
    // The user must have rights for restart service
    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
    //  run('sudo systemctl restart php-fpm.service');
});
after('deploy:symlink', 'php-fpm:restart');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

/**
 * Main task
 */
task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'deploy:symlink',
    'cleanup',
    'artisan:cache:clear',
    'artisan:config:cache',
])->desc('Deploy your project');

after('deploy', 'success');