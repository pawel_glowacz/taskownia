/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 256);
/******/ })
/************************************************************************/
/******/ ({

/***/ 256:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(257);


/***/ }),

/***/ 257:
/***/ (function(module, exports) {



$(document).ready(function () {
    makeDraggable();
    function makeDraggable() {
        $('aside .items .item').each(function () {
            $(this).data('event', {
                title: $(this).data('title'),
                id: Math.floor(Math.random() * 100 + 1000),
                client: $(this).data('client'),
                deadline: $(this).data('deadline'),
                duration: $(this).data('duration'),
                duration2: $(this).data('duration'),
                project_id: $(this).data('project_id'),
                project_id2: $(this).data('project_id'),
                color: '#096709',
                stick: true
            });
            $(this).draggable({
                zIndex: 999,
                revert: true,
                revertDuration: 1
            });
        });
    }

    jQuery.fn.preventDoubleSubmission = function () {
        $(this).on('submit', function (e) {
            var $form = $(this);
            if ($form.data('submitted') === true) {
                e.preventDefault();
            } else {
                $form.data('submitted', true);
            }
        });
        return this;
    };
    $('form').preventDoubleSubmission();

    $('.select2').select2();

    $('#templates').on('shown.bs.collapse', function () {
        localStorage.setItem('templates', true);
        $(this).show();
    });
    $('#templates').on('hidden.bs.collapse', function () {
        localStorage.setItem('templates', false);
        $(this).hide();
    });
    var templates = localStorage.getItem('templates');
    if (templates == "true") {
        $('#templates').addClass('show');
        $('#templates').addClass('in');
    } else {
        $('#templates').removeClass('show');
        $('#templates').removeClass('in');
    }

    $('#waiting').on('shown.bs.collapse', function () {
        localStorage.setItem('waiting', true);
        $(this).show();
    });
    $('#waiting').on('hidden.bs.collapse', function () {
        localStorage.setItem('waiting', false);
        $(this).hide();
    });
    var waiting = localStorage.getItem('waiting');
    if (waiting == "true") {
        $('#waiting').addClass('show');
        $('#waiting').addClass('in');
    } else {
        $('#waiting').removeClass('show');
        $('#waiting').removeClass('in');
    }

    $('#users-list').on('shown.bs.collapse', function () {
        localStorage.setItem('users-list', true);
        $(this).show();
    });
    $('#users-list').on('hidden.bs.collapse', function () {
        localStorage.setItem('users-list', false);
        $(this).hide();
    });
    var usersList = localStorage.getItem('users-list');
    if (usersList == "true") {
        $('#users-list').addClass('show');
        $('#users-list').addClass('in');
    } else {
        $('#users-list').removeClass('show');
        $('#users-list').removeClass('in');
    }

    $('#usersBtn').click(function () {
        $('#users-list').toggle();
    });
    $('#add_task').click(function () {
        $('#add-task-modal').show();
    });
    $('#add_user').click(function () {
        $('#add-user-modal').show();
    });
    $('.modals .buttons .btn-no').click(function () {
        $(this).closest('.modals').hide();
    });
    $('#profile-btn').click(function () {
        $('#profile-view').show();
    });
    $('#attach').change(function () {
        $('#attach').submit();
    });

    $("input:file").change(function () {
        var fileName = $(this).val();
        $(".add-file").html(fileName);
    });
    $('.edit').click(function () {
        $(this).parent().find('input').prop('disabled', false);
    });
    $('#user_history_hide').click(function () {
        $('#user_history').toggle();
    });
    $('#task_history_hide').click(function () {
        $('#task_history').toggle();
    });
    $('#task_attach_hide').click(function () {
        $('#task_attach').toggle();
    });
});

/***/ })

/******/ });