
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
Vue.use(require('vue-resource'));
window.moment = require('moment');
window.moment = require('moment-timezone');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.http.interceptors.push((request, next) => {
    request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

next();
});


Vue.component('calendar', require('./components/calendar.vue'));
Vue.component('addtask', require('./components/task/addTask.vue'));
Vue.component('sidetask', require('./components/sidebar/sidetask.vue'));

//Profile
Vue.component('creditionals', require('./components/profile/creditionals.vue'));
Vue.component('profileheader', require('./components/profile/header.vue'));
Vue.component('profilehistory', require('./components/profile/history.vue'));
Vue.component('timetable', require('./components/profile/timetable.vue'));

//Partials
Vue.component('FormError', require('./components/modals/formError.vue'));
Vue.component('footbar', require('./components/footer/footer.vue'));
Vue.component('headbar', require('./components/header/header.vue'));
Vue.component('userhours', require('./components/profile/userHours.vue'));

//Admin
// Vue.component('admin', require('./components/admin/dashboard.vue'));
Vue.component('adminsidebar', require('./components/admin/sidebar.vue'));
Vue.component('adminaddhours', require('./components/admin/addHours.vue'));
Vue.component('admindeadline', require('./components/admin/addHours.vue'));
Vue.component('adminoptions', require('./components/admin/options.vue'));
Vue.component('adduser', require('./components/admin/addUser.vue'));
Vue.component('users', require('./components/sidebar/users.vue'));

//User
Vue.component('useraddhours', require('./components/user/addHours.vue'));
Vue.component('usersubtask', require('./components/user/subTask.vue'));


//Task
Vue.component('task', require('./components/task/task.vue'));
Vue.component('taskattachments', require('./components/task/taskAttachments.vue'));
Vue.component('taskhistory', require('./components/task/taskHistory.vue'));
Vue.component('tasknotes', require('./components/task/taskNotes.vue'));
Vue.component('taskheader', require('./components/task/taskHeader.vue'));
Vue.component('search', require('./components/task/search.vue'));


const app = new Vue({
    el: '#app',
    data: {
    },
    method: {

    },
    mounted: function () {

    }
});

