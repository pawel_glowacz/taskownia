export default {

    data: {
        userId: '',
        task: '',
        taskNotes: '',
        taskNoteBody: '',
        taskLogs: '',
        taskAttachments: '',
        file: '',
        event: '',
        actualEvent: '',
        errors: [],
        userWorkingHours: [],
        userWorkingHoursSide: [],
        timer: '',
        businessHours: {
            globalStart: '',
            globalEnd: ''
        },
        templates: '',
        waitings: '',
        title: '',
        client: '',
        duration: '',
        project_id: '',
        duration_days: '',
        duration_minutes: '',
        duration_hours: '',
        selected: '',
        date: '',
        time: '',
    },

    fetchData() {
        // fetch logic
    }

}