<div class="col-lg-4 creditionals">
    @if(!empty($admin->id))
        <form method="post" action="{{ route('edit.admin.creditionals') }}" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-9 personal">
                    <div class="name">
                        <label for="first_name">Imię</label> <br>
                        <input type="text" name="first_name" disabled="disabled" value="{{ $admin->first_name }}">
                        <div class="edit">Edytuj</div>
                    </div>
                    <div class="surname">
                        <label for="second_name">Nazwisko</label><br>
                        <input type="text" name="second_name" disabled="disabled" value="{{ $admin->second_name }}">
                        <div class="edit">Edytuj</div>

                    </div>
                </div>
                <div class="col-lg-3 photo">
                    <div class="photo">
                        <h5>Zdjęcie</h5>

                        <label for="upload-photo">
                            <img src="{{ asset('img/photos')}}/{{$admin->photo OR "profile.jpg"}}"
                                 alt="">

                            <div class="add-file">
                                zmień zdjęcie
                            </div>
                        </label>
                        <input type="file" name="photo" id="upload-photo">
                    </div>

                </div>
                <div class="personal admin col-lg-12">

                    <div class="login">
                        <label for="login">Login</label><br>
                        <input type="text" name="nickname" disabled="disabled" value="{{ $admin->nickname }}">
                        <div class="edit">Edytuj</div>

                    </div>

                    <div class="password">
                        <label for="password">Hasło</label><br>
                        <input type="password" name="password" disabled="disabled">
                        <div class="edit">Edytuj</div>

                    </div>
                    {{ csrf_field() }}
                    <div class="save">
                        <button type="submit" name="update_user" class="btn-gray updateUser">ZAPISZ ZMIANY</button>
                    </div>
                </div>
            </div>
        </form>
@endif
</div>