@extends('layouts.login')
@section('content')
    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">Zaloguj się (administrator)</div>
            <div class="card-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.auth') }}">
                    {!! csrf_field() !!}
                    <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label">E-Mail</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="login" value="{{ old('login') }}">
                            @if ($errors->has('login'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('login') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label">Hasło</label>
                        <div class="col-md-12">
                            <input type="password" class="form-control" name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-md-offset-3">
                            <button type="submit" class="btn btn-block btn-danger">Zaloguj</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection