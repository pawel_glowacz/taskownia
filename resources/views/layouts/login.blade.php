<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Taskownia | Panel logowania </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ URL::to('css/frontend.css') }}">
    <style>
        body{
            color: black;
        }
        .card{
            width:400px;
        }
    </style>
</head>
<body class="bg-dark">
@yield('content')
</body>
</html>