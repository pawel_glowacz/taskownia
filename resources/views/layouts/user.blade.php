<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Taskownia</title>
    <link rel="stylesheet" href="{{ URL::to('js/fullcalendar-3.6.2/fullcalendar.min.css') }}">
    {{--    <link rel="stylesheet" href="{{ URL::to('js/fullcalendar-3.6.2/fullcalendar.print.min.css') }}">--}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ URL::to('css/frontend.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('styles')
</head>
<body>
<div id="app">
    <div id="wrapper">
        @yield('content')
    </div>
</div>
<script>
    var adminId = {{ !empty($admin->id) ? $admin->id : "false" }};
    var userId = {{ !empty($user->id) ? $user->id : "false" }};
</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/front.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<script src="{{ URL::to('js/fullcalendar-3.6.2/lib/moment.min.js') }}"></script>
<script src='{{ URL::to('js/fullcalendar-3.6.2/locale/pl.js') }}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>


@yield('scripts')
</body>
</html>