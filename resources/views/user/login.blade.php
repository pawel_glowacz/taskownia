@extends('layouts.login')
@section('content')
    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">Zaloguj się do Taskowni!</div>
            <div class="card-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('user.auth') }}">
                    {!! csrf_field() !!}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label">E-Mail</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label">Hasło</label>
                        <div class="col-md-12">
                            <input type="password" class="form-control" name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-md-offset-3">
                            <button type="submit" class="btn btn-block btn-success">Zaloguj</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection