<div class="col-lg-6 timetable-wrapp">

    <div class="header">
        <div class="previous">
            <button class="btn-light-gray prevBtn">poprzedni</button>
        </div>
        <div class="text">
            HARMONOGRAM PRACY
        </div>
    </div>

    <div class="timetables">
    <div class="timetables-wrap">
        <div id="loader">
            <img src="{{ asset('img/loader.gif')}}" alt="">
        </div>
        <div class="row">
            <div class="timetable-box col-lg-12 col-xl-6">
                <div class="timetable" id="week0" data-week="0">
                    <div class="date" id="date0" data-date="0">
                    </div>
                    <div class="days">
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    pon.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    wt.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00'),'18:00' }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    śr.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    czw.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    pt.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    sob.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    nd.
                                </div>
                                <div class="col-lg-5"> od
                                    {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do
                                    {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>

                        <div class="save">
                            <button class="btn-gray saveTimetable">ZAPISZ</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="timetable-box col-lg-12 col-xl-6">
                <div class="timetable" id="week1" data-week="1">
                    <div class="date" id="date1" data-date="1" >
                    </div>
                    <div class="days">
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    pon.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    wt.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00'),'18:00' }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    śr.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    czw.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    pt.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    sob.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    nd.
                                </div>
                                <div class="col-lg-5"> od
                                    {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do
                                    {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>

                        <div class="save">
                            <button class="btn-gray saveTimetable">ZAPISZ</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="timetable-box col-lg-12 col-xl-6">
                <div class="timetable" id="week2" data-week="2">
                    <div class="date" id="date2" data-date="2">
                    </div>
                    <div class="days">
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    pon.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    wt.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00'),'18:00' }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    śr.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    czw.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    pt.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    sob.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    nd.
                                </div>
                                <div class="col-lg-5"> od
                                    {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do
                                    {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>

                        <div class="save">
                            <button class="btn-gray saveTimetable">ZAPISZ</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="timetable-box col-lg-12 col-xl-6">
                <div class="timetable" id="week3" data-week="3">
                    <div class="date" id="date3" data-date="3">
                    </div>
                    <div class="days">
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    pon.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    wt.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00'),'18:00' }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    śr.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    czw.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    pt.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    sob.
                                </div>
                                <div class="col-lg-5"> od {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>
                        <div class="day">
                            <div class="row">
                                <div class="col-lg-2">
                                    nd.
                                </div>
                                <div class="col-lg-5"> od
                                    {{ Form::selectUserHours('start','10:00') }}
                                </div>
                                <div class="col-lg-5"> do
                                    {{ Form::selectUserHours('end','18:00') }}
                                </div>
                            </div>
                        </div>

                        <div class="save">
                            <button class="btn-gray saveTimetable">ZAPISZ</button>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        </div>
    </div>
    <div class="next">
        <button class="btn-light-gray nextBtn">następny</button>
    </div>
</div>
@section('scripts')
    <script>

        var data1 = [];


        $('.saveTimetable').click(function () {
            $(this).closest('.days').children('.day').each(function () {
                var start = $(this).find('select[name="start"]').data('date') + " " + $(this).find('select[name="start"]').val();
                var end = $(this).find('select[name="end"]').data('date') + " " + $(this).find('select[name="end"]').val();
                data1.push(
                    {"start": start, "end": end}
                );
            });

//            console.log(data1);

            var formData = {
                data: data1
            };
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                beforeSend: function(){
                    $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
                url: "{{ route('update.timetable',['userId'=>1]) }}",
                data: formData,
                dataType: 'json',
                success: function (data) {
                },
                error: function (data) {
                    alert('Błąd!');
                    console.log('Error:', data);
                }
            });
        });
        var a=0;
        var b=5;
        calculateDays(a, b);

        function calculateDays(a, b) {
            var data2 =[];
            var tab =[];
            var weekStart = new Array();
            var weekEnd = new Array();
            var l;
            for (l = a; l < b; l++) {
                weekStart[l] = moment().add(l, 'weeks').startOf('week');
                weekEnd[l] = moment().add(l, 'weeks').endOf('week').format('DD.MM.YYYY');
                $('.date[data-date="' + l + '"]').html(weekStart[l].format('DD.MM.YYYY') + ' - ' + weekEnd[l]);
                var current = 0;
                $('.timetable[data-week="' + l + '"]').find('.day').each(function () {
                    $(this).find('select').each(function () {
                        var day = moment().add(l, 'weeks').startOf('week').add(current, 'days');
                        $(this).attr('data-date', day.format('DD.MM.YYYY'));
                        // var start= day.format('YYYY-MM-DD') + ' ' + $(this).val() + ':00';
                        var start= day.format('YYYY-MM-DD') + ' ' +  '10:00';
                        var end= day.format('YYYY-MM-DD') + ' ' + '18:00';

                        if ($(this).attr('name') == "start" ) {
                            tab.push(
                                {"start": start, "end": end}
                            );
                        }
                    });
                    current++;
                });
            }
            var formData = {
                data: tab
            };
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                cache: false,
                beforeSend: function(){
                    $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
                url: "{{ route('get.timetable',['userId'=>1]) }}",
                data: formData,
                dataType: 'json',
                success: function (data) {
                    $.each(data['data'],function(propName,propVal){
                        var day = moment(propVal['start']).format('DD.MM.YYYY');
                        var start = moment(propVal['start']).format('HH:mm');
                        var end = moment(propVal['end']).format('HH:mm');
                        // console.log(propVal['start']);
                        // console.log(propVal['end']);
//                        console.log(day);
//                        console.log(start + "+" + end);
                        $("ul").find("[data-slide='" + current + "']");

                        $('.timetables').find("[data-date='"+ day +"']").each(function(){
                            // console.log(start);
                            // console.log(end);
                            // console.log(day);
                            if ($(this).attr('name') == "start" ){
                                 $(this).val(start);
                            }else if($(this).attr('name') == "end" ){
                                $(this).val(end);
                            }
                        });

                    });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
            return data2;
        }
        $('.nextBtn').click(function () {
            $('.timetable').each(function () {
                var last= $(this).attr('data-week');
                $(this).attr('data-week', parseInt(last)+4);
            });
            $('.date').each(function () {
                var last= $(this).attr('data-date');
                $(this).attr('data-date', parseInt(last)+4);
            });
            a = a + 4;
            b = b + 4;
            calculateDays(a, b);
        });
        $('.prevBtn').click(function () {
            $('.timetable').each(function () {
                var last= $(this).attr('data-week');
                $(this).attr('data-week', parseInt(last)-4);
            });
            $('.date').each(function () {
                var last= $(this).attr('data-date');
                $(this).attr('data-date', parseInt(last)-4);
            });
            a = a - 4;
            b = b - 4;
            calculateDays(a, b);
        });
    </script>
@endsection