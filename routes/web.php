<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Admin\AdminLoginController@getAdminLogin');


Route::get('/user', ['as' => 'user.auth', 'uses' => 'User\UserLoginController@getUserLogin']);
Route::post('/user', ['as' => 'user.auth', 'uses' => 'User\UserLoginController@userAuth']);

Route::get('/admin', ['as' => 'admin.login', 'uses' => 'Admin\AdminLoginController@getAdminLogin']);
Route::post('/admin', ['as' => 'admin.auth', 'uses' => 'Admin\AdminLoginController@adminAuth']);

Route::get('/logout', ['as' => 'logout', 'uses' => 'User\UserLoginController@logout']);


Route::group([
    'prefix' => '/admin',
    'middleware' => 'authadmin'
], function () {
    Route::get('/dashboard', [
        'uses' => 'Admin\AdminController@getIndex',
        'as' => 'admin.dashboard'
    ]);
    Route::get('/dashboard/{userId}', [
        'uses' => 'Admin\AdminController@getIndexUser',
        'as' => 'admin.dashboard.user'
    ]);
    Route::get('/dashboard/{userId}/{taskId}', [
        'uses' => 'Admin\TaskController@getTaskUser',
        'as' => 'admin.dashboard.user.task'
    ]);

    Route::get('/calendar/load/task/{userId}/{taskId}', [
        'uses' => 'Admin\TaskController@loadTaskUser',
        'as' => 'admin.dashboard.load.task'
    ]);
    Route::post('/users/all', [
        'uses' => 'Admin\UserController@getUsers',
        'as' => 'get.users'
    ]);
    Route::post('/user/id', [
        'uses' => 'Admin\UserController@getUserId',
        'as' => 'get.user.id'
    ]);
    Route::post('/user/add', [
        'uses' => 'Admin\UserController@addUser',
        'as' => 'add.user'
    ]);
    Route::post('/edit/user', [
        'uses' => 'Admin\UserController@editUserCreditionals',
        'as' => 'edit.user.creditionals'
    ]);
    Route::post('/edit/user/hours/{userId}', [
        'uses' => 'Admin\UserController@editUserHours',
        'as' => 'edit.user.hours'
    ]);
    Route::post('/edit/admin', [
        'uses' => 'Admin\AdminController@editAdminCreditionals',
        'as' => 'edit.admin.creditionals'
    ]);
    Route::post('/task/add', [
        'uses' => 'Admin\TaskController@addTask',
        'as' => 'add.task'
    ]);
    Route::post('/task/delete', [
        'uses' => 'Admin\TaskController@deleteTask',
        'as' => 'delete.task'
    ]);
    Route::post('/task/edit', [
        'uses' => 'Admin\TaskController@editTask',
        'as' => 'edit.task'
    ]);
    Route::post('/task/get', [
        'uses' => 'Admin\TaskController@getTaskId',
        'as' => 'get.task'
    ]);
    Route::post('/task/sidebar', [
        'uses' => 'Admin\TaskController@getSidebarTasks',
        'as' => 'get.task.sidebar'
    ]);
    Route::post('/task/note/add', [
        'uses' => 'Admin\TaskController@addTaskNote',
        'as' => 'add.task.note'
    ]);
    Route::post('/task/note/edit/{taskNoteId}', [
        'uses' => 'Admin\TaskController@editTaskNote',
        'as' => 'edit.task.note'
    ]);
    Route::post('/task/note/delete', [
        'uses' => 'Admin\TaskController@deleteTaskNote',
        'as' => 'delete.task.note'
    ]);
    Route::post('/task/file/add', [
        'uses' => 'Admin\TaskController@addTaskFile',
        'as' => 'add.task.file'
    ]);
    Route::post('/task/pause', [
        'uses' => 'Admin\TaskController@pauseTask',
        'as' => 'task.pause'
    ]);
    Route::post('/task/stop', [
        'uses' => 'Admin\TaskController@stopTask',
        'as' => 'task.stop'
    ]);
    Route::post('/task/start', [
        'uses' => 'Admin\TaskController@startTask',
        'as' => 'task.start'
    ]);
    Route::post('/task/file/edit/{taskFileId}/{taskId}', [
        'uses' => 'Admin\TaskController@editTaskFile',
        'as' => 'edit.task.file'
    ]);
    Route::get('/task/file/download/{id}', [
        'uses' => 'Admin\TaskController@downloadTaskFile',
        'as' => 'download.task.file'
    ]);
    Route::post('/task/file/delete', [
        'uses' => 'Admin\TaskController@deleteTaskFile',
        'as' => 'delete.task.file'
    ]);
    Route::post('/user/timetable', [
        'uses' => 'Admin\UserController@getTimetable',
        'as' => 'get.timetable'
    ]);
    Route::post('/user/timetablehours', [
        'uses' => 'Admin\UserController@getTimetableHours',
        'as' => 'get.timetablehours'
    ]);
    Route::post('/user/update/timetable', [
        'uses' => 'Admin\UserController@updateTimetable',
        'as' => 'update.timetable'
    ]);
    Route::post('/user/get/tasks', [
        'uses' => 'Admin\TaskController@getTasks',
        'as' => 'user.get.tasks'
    ]);
    Route::post('/user/get/background/{userId}', [
        'uses' => 'Admin\TaskController@getBackground',
        'as' => 'user.get.background'
    ]);
    Route::post('/user/edit/event/calendar', [
        'uses' => 'Admin\TaskController@editEventCalendar',
        'as' => 'user.edit.event.calendar'
    ]);
    Route::post('/user/add/event/calendar', [
        'uses' => 'Admin\TaskController@addEventCalendar',
        'as' => 'user.add.event.calendar'
    ]);
    Route::post('/task/search/', [
        'uses' => 'Admin\TaskController@searchTask',
        'as' => 'task.search'
    ]);
    Route::get('/options/hours/', [
        'uses' => 'Admin\OptionController@getHours',
        'as' => 'option.hours'
    ]);
    Route::post('/options/hours/update', [
        'uses' => 'Admin\OptionController@updateHours',
        'as' => 'option.hours.update'
    ]);

});

Route::group([
    'prefix' => '/user',
    'middleware' => 'authuser'
], function () {
    Route::get('/dashboard', [
        'uses' => 'User\UserController@getIndex',
        'as' => 'user.dashboard'
    ]);
});
