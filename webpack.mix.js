let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.webpackConfig({
    resolve: {
        alias: {
            'jquery-ui': 'jquery-ui-dist/jquery-ui.js'
        }
    }
});

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/main.scss', 'public/css');

mix.js('resources/assets/js/front.js', 'public/js')
    .js('resources/assets/js/socket.js', 'public/js')
    .sass('resources/assets/sass/frontend.scss', 'public/css')
    .options({
        processCssUrls: false
    });

mix.browserSync({
    proxy: 'taskownia.test'
});